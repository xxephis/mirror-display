'use strict';

/**
 * @ngdoc function
 * @name mirrorDisplayApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mirrorDisplayApp
 */
angular.module('mirrorDisplayApp')
  .controller('MainCtrl', function ($scope, $http, $routeParams, $interval, $sce) {
    $scope.myAudioStreams = [
      ["Illegal Operation","http://i1.theportalwiki.net/img/b/bd/Turret_turret_pickup_5.wav"],
      ["Kill 1","http://i1.theportalwiki.net/img/a/a9/GLaDOS_escape_00_part2_nag01-1.wav"],
      ["Kill 2","http://i1.theportalwiki.net/img/2/29/GLaDOS_escape_01_first_hit_nag03-1.wav"],
      ["Kill 3","http://i1.theportalwiki.net/img/2/27/GLaDOS_fgbwheatleyentrance10.wav"],
      ["Radio Bob","http://bob.hoerradar.de/radiobob-live-mp3-hq"]
    ];
    $scope.myAudoStreamID = -1;

    //$scope.picUrl = "http://www.niederschlagsradar.de/images.aspx?jaar=-6&type=deutschland.precip&cultuur=de-DE&continent=deutschland&datum=201604220000";
    $interval(function () {
      //REGENRADAR
      var URLTime = new moment().subtract(moment().format("HH")%3,'hours'); //Startzeit
      URLTime.startOf('hour');//gerundet auf Stunde
      URLTime.add(moment().format("ss")%16*3, 'hours');//Wechsel im Sekundentakt
      //console.log(URLTime.format("YYYYMMDDHH[00]"));//Das Format brauchen wir
      var newUrl = "http://www.niederschlagsradar.de/images.aspx?jaar=-6&type=deutschland.precip&cultuur=de-DE&continent=deutschland&datum="+URLTime.format("YYYYMMDDHH[00]");
      $scope.myPicUrl = newUrl;
      //ZEIT
      $scope.myDate = moment().format('Do MMMM YYYY, HH:mm:ss');
    }, 1000);

    $scope.$on('key-pressed', function (event) {
      $scope.myAudoStreamID = ($scope.myAudoStreamID+1)%($scope.myAudioStreams.length);
      var myAudioStream = $scope.myAudioStreams[$scope.myAudoStreamID];
      $scope.myAudioStreamSrc = $sce.trustAsResourceUrl(myAudioStream[1]);
      $scope.myAudioStreamSrcString = myAudioStream[0];
      //console.log($scope.myAudioStreams[$scope.myAudoStreamID]);
    });



  });
