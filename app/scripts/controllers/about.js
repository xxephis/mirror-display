'use strict';

/**
 * @ngdoc function
 * @name mirrorDisplayApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the mirrorDisplayApp
 */
angular.module('mirrorDisplayApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
