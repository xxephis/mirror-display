'use strict';

/**
 * @ngdoc overview
 * @name mirrorDisplayApp
 * @description
 * # mirrorDisplayApp
 *
 * Main module of the application.
 */
angular
  .module('mirrorDisplayApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).controller('KeyCtrl', function ($scope) {
    $scope.keypressed =function (event) {
      //console.log("press");
      $scope.$broadcast('key-pressed', event);
    };
  });
